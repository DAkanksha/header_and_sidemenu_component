import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { AppHeaderComponent } from './widgets/app-header/app-header.component';
import { MenuComponent } from './widgets/menu/menu.component';

@NgModule({
  declarations: [
    AppComponent,
    AppHeaderComponent,
    MenuComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
