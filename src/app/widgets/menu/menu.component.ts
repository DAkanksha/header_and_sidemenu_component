import { Component, OnInit, Input } from '@angular/core';
import { UserModel } from '../../models/user.model';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})

export class MenuComponent implements OnInit {
  private user: UserModel = new UserModel();

  @Input() private links: Array<any> = [];

  constructor() { }

  ngOnInit() { }
}
